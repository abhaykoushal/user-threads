#include <setjmp.h>

#define EXECUTING 0
#define READY 1
#define COMPLETED 2
#define WAITING 3

#define MAX_THREADS 100

typedef struct itimerval athread_timer_t;

typedef pid_t athread_t;

typedef struct {
	athread_t tid;
	//athread_t ktid;
	int state;
	sigjmp_buf context;
	void *stackbase;
	int stacksize;
	void *args;
	void *res;
	void *(*func) (void *);
	int joinable;				//JOIN INFO
	athread_t joined_on;
	sigset_t sighandler;			//SIGNAL INFO
}athread;

typedef struct node {
	athread *tcb;
	struct node *next;
}threadNode;

typedef struct {
	int count;
	threadNode *head;
	threadNode *tail;
} queue;

typedef struct{
	size_t stackSize;
	void* stackBase;
	int joinable;
} athread_attr_t;


void init(queue *Q);
int isEmpty(queue *Q);
void enqueue(queue *Q, athread *tcb);
athread *dequeue(queue *Q);
void display(queue *Q);
athread *search(queue *Q, pid_t id);

int athread_init(void);
int athread_create(athread_t *thread, const athread_attr_t *attr, void *(*start_routine)(void *), void *arg);
void scheduler(int sig);
void mthread_start(void);
void enableInterrupt(athread_timer_t *T) ;
void disableInterrupt(athread_timer_t *T) ;
