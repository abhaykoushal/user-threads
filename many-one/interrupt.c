#include <signal.h>
void interrupt_enable(athread_timer_t *T) {
    T->it_value.tv_sec = 0;
    T->it_interval.tv_sec = 0;
    T->it_interval.tv_usec = 12000;
    timer->it_value.tv_usec = 12000;
    setitimer(ITIMER_VIRTUAL, T, 0);
}

void interrupt_disable(mthread_timer_t *T) {
    /* Disable timer */
    T->it_value.tv_sec = 0;
    T->it_interval.tv_sec = 0;
    T->it_value.tv_usec = 0;
    T->it_interval.tv_usec = 0;
    setitimer(ITIMER_VIRTUAL, T, 0);
}




