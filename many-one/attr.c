#include<stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include "userthreads.h"
#include "stack.h"


int mthread_attr_init(mthread_attr_t * attr){
	if(!attr)
		return EINVAL;
	attr->stackBase = NULL;
	attr->stackSize = stackSize();
	return 0;
}

int mthread_attr_destroy(mthread_attr_t* attr){
	if (!attr)
		return EINVAL;
	free(attr);
	return 0;
}

int mthread_attr_getstacksize(mthread_attr_t* attr, size_t * sz){
	if (!attr)
		return EINVAL;
	*sz =  (attr->stackSize);
	return 0;
}

int mthread_attr_setstacksize(mthread_attr_t* attr, size_t sz){
        if (!attr)
                return EINVAL;
        (attr->stackSize) = sz;
        return 0;
}


int mthread_attr_getstackaddr(mthread_attr_t* attr, void **sB){
	if (!attr)
		return EINVAL;
	*sB = attr->stackBase;
	return 0;
}

int mthread_attr_setstackaddr(mthread_attr_t* attr, void *sB){
	if (!attr)
		return EINVAL;
	attr->stackBase = sB;
	return 0;
}

int main(){
	mthread_attr_t th;
	int d = mthread_attr_init(&th);
	printf("%ld\n", stackSize());
	size_t SIZE;
	mthread_attr_getstacksize(&th, &SIZE);
	printf("H%ld\n", SIZE);
	SIZE = 10000;
	mthread_attr_setstacksize(&th, SIZE);
	mthread_attr_getstacksize(&th, &SIZE);
	printf("O%ld", SIZE);
	return 0;
}
