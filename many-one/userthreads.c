#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "userthreads.h"
#include "queue.h"
#include "stack.h"
#include <signal.h>
#include <setjmp.h>
#include <sys/time.h>


queue threadQ;
static athread *curr;
static uid = 0;
static athread_timer_t timer;


static athread * getnext(void) {
    athread *runner;
    int i = getCount(&threadQ);

    while(i--) {
        runner = dequeue(&threadQ);

        switch(runner->state) {
            case READY:
                return runner;
            case WAITING:
            case COMPLETED:
                enqueue(&threadQ, runner);
                break;
            case EXECUTING:
                return NULL;
        }
    }

    return NULL;
}
void enableInterrupts(athread_timer_t *T) {
    T->it_value.tv_sec = 0;
    T->it_interval.tv_sec = 0;
    T->it_interval.tv_usec = 12000;
    T->it_value.tv_usec = 12000;
    setitimer(ITIMER_VIRTUAL, T, 0);
}

void disableInterrupts(athread_timer_t *T) {
    T->it_value.tv_sec = 0;
    T->it_interval.tv_sec = 0;
    T->it_value.tv_usec = 0;
    T->it_interval.tv_usec = 0;
    setitimer(ITIMER_VIRTUAL, T, 0);
}


int athread_init(void){
	init(&threadQ);
	curr = (athread*)malloc(sizeof(athread));
	curr->tid = uid++;
	curr->state = EXECUTING;
	curr->joined_on = -1;
	curr->func = NULL;
	curr->args = NULL;
       	curr->res = NULL;
	struct sigaction signal_handler;
	sigset_t mask;
	sigfillset(&mask);
	signal_handler.sa_flags = 0;
	signal_handler.sa_handler = scheduler;
	signal_handler.sa_mask = mask;
	sigaction(SIGVTALRM, &signal_handler, NULL);
	enableInterrupts(&timer);
	return 0;

}

int athread_create(athread_t *thread, const athread_attr_t *attr, void *(*start_routine)(void *), void *arg) {
	athread *t = (athread*)(malloc(sizeof(athread)));
	disableInterrupt(&timer);
	t->tid            = uid++;
    	t->func           = start_routine;
    	t->args            = arg;
    	t->joined_on      = -1;
	t->state          = READY;
    	sigemptyset(&t->sighandler);
	t->stacksize = (attr) ?  attr->stackSize : stackSize();
	t->stackbase = (attr) ?  attr->stackBase : NULL;
	if( !t->stackbase ){
		t->stackbase = stackAllocate(t->stacksize);
		if( !t->stackbase){
			enableInterrupt(&timer);
			return EAGAIN;
		}
	}

	t->joinable = attr ? attr->joinable : 1;
	sigsetjmp(t->context, 1);
	enqueue(&threadQ, t);
	enableInterrupt(&timer);
	return 0;
}

void scheduler(int sig){

	disableInterrupt(&timer);
	
	if ( sigsetjmp(curr->context, 1))
		return;
	
	if(curr->state == EXECUTING)
		curr->state = READY;
	
	enqueue(&threadQ, curr);

	if ( (curr = getnext()) == NULL)
		exit(0);
	curr->state = EXECUTING;

	for(int i = 0; i < NSIG; i++){
		if(sigismember(&curr->sighandler, sig)){
			raise(sig);
			sigdelset(&curr->sighandler, sig);
		}
	}
	enableInterrupt(&timer);
	siglongjmp(curr->context, 1);
	
}

void mthread_start(void) {
    curr->res = curr->func(curr->args);
    return;
}

void *func2(void *arg){
         int n = 10;
         int tid = gettid();
         while(n--)
         printf("CREATE FUNC2 THREAD WORKING PROPERLY\n");
}


void *func(void *arg){
          for(int i = 0; i < 15; i++){
               printf("CREATE THREAD WORKING PROPERLY\n");
    }
  }

int main(){
	athread_t th, th2;
	athread_create(&th, NULL, func, NULL);
	athread_create(&th2, NULL, func2, NULL);
}	  
